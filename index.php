
<?php

require 'vendor/autoload.php';

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Logging\LoggingClient;
use Google\Cloud\SecretManager\V1\SecretManagerServiceClient;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use SendGrid\Mail\Attachment;
use SendGrid\Mail\Mail;
use Slim\Factory\AppFactory;

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$app = AppFactory::create();
$app->addRoutingMiddleware();
$logging = new LoggingClient([
    'projectId' => $_ENV['GOOGLE_PROJECT_ID']
]);
$logger = $logging->psrLogger('app');

$app->post('/', function (Request $request, Response $response) use ($logger) {
    register_stream_wrapper($_ENV['GOOGLE_PROJECT_ID']);

    $body = $request->getBody()->getContents();
//    $logger->info($body);
    $decodedJson = json_decode($body, true);

    // check if message type is new file in the bucket
    if ($decodedJson['message']['attributes']['eventType'] == 'OBJECT_FINALIZE') {
        $decodedData = json_decode(base64_decode($decodedJson['message']['data']));
        $link = sprintf('https://storage.cloud.google.com/%s/%s',$decodedData->bucket,$decodedData->name);
        $logger->info($link);
        $gsPath = sprintf('gs://%s/%s',$decodedData->bucket,$decodedData->name);
        $logger->info($gsPath);
        // get required data from the filename
        $fileNameArray = explode('/', $decodedData->name);
        $fileName = $fileNameArray[count($fileNameArray)-1];
        $propertyId = explode(' ', $fileName)[0];
        $fileContents = file_get_contents($gsPath);
        sendEmail($link, $logger, $fileContents, $fileName, $propertyId);

        return $response->withStatus(200);
    }

    return $response->withStatus(204);
});
$app->run();


function register_stream_wrapper($projectId)
{
    $client = new StorageClient(['projectId' => $projectId]);
    $client->registerStreamWrapper();
}

function sendEmail(string $link, $logger, $fileContents, $fileName, $propertyId) {

    $email = new Mail();
    $email->setFrom($_ENV['EMAIL_FROM_ADDRESS'], $_ENV['EMAIL_FROM_NAME']);
    $email->setSubject("Document Upload for $propertyId");
    $email->addTo($_ENV['EMAIL_TO_ADDRESS'], $_ENV['EMAIL_TO_NAME']);
    $email->addContent(
        "text/html", $_ENV['EMAIL_CONTENT_INTRO'] . $propertyId
    );

    try {
        $attachment = new Attachment();
        $attachment->setContent($fileContents);
        $attachment->setType('application/pdf');
        $attachment->setFilename($fileName);
        $attachment->setDisposition("attachment");
        $email->addAttachment($attachment);
    } catch (Exception $e) {
        $logger->error('Caught exception: '. $e->getMessage());
    }

    $sendgrid = new \SendGrid(get_sendgrid_api_key());
    try {
        $response = $sendgrid->send($email);
        $logger->info($response->statusCode());
        $logger->info($response->headers());
        $logger->info($response->body());
    } catch (Exception $e) {
        $logger->error('Caught exception: '. $e->getMessage());
    }
}

function get_sendgrid_api_key() {
    $client = new SecretManagerServiceClient();
    $name = $client->secretVersionName($_ENV['GOOGLE_PROJECT_ID'], 'SENDGRID_API_KEY', 'latest');
    $response = $client->accessSecretVersion($name);
    $payload = $response->getPayload()->getData();

    return $payload;
}