# Document Upload Notifier

### Architecture
GCS Bucket -> (notification) -> PubSub Topic -> PubSub Push Subsciption -> App Engine handler -> SendGrid Email

using Secrets Manager to store "SENDGRID_API_KEY"

### Steps
0. fill out .env file with the required variables / change php.ini
1. composer install
2. gcloud app deploy
3. get app url
4. create PubSub topic
5. create PubSub push subscription
6. create a correctly named secret with the Sendgrid Api Key
7. add IAM roles
8. upload a file to the bucket

### IAM additonal requirements
Once Bucket Notification is Enabled
service-${PROJECT_NUMBER}@gs-project-accounts.iam.gserviceaccount.com  => Pub/Sub Publisher

Once Secrets Manager has been setup and a secret with name "SENDGRID_API_KEY" stored
Add extra permission to
${PROJECT_ID}@appspot.gserviceaccount.com  (App Engine default service account) =>
Secret Manager Secret Accessor
